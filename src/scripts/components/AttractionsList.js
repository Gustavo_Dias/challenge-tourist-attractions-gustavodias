export class AttractionsList {

    constructor() {

        this.list = []; //ARRAY VAZIO
        this.selectors();
        this.events();
    }

    selectors() {
        this.form = document.querySelector(".section-add-input");
        this.titleInput = document.querySelector(".input-title");
        this.descriptionInput = document.querySelector(".input-description");
        this.imageInput = document.querySelector(".input-image-input");
        this.imageDiv = document.querySelector(".input-image-div");
        this.attractions = document.querySelector(".list");
        this.classImage = document.querySelector(".image");
        this.placeholderImage = document.querySelector(".placeholder-image");
    }

    events() {
        this.form.addEventListener("submit", this.addAttractionToList.bind(this));
        this.imageInput.addEventListener("change", this.previewImage.bind(this));
        this.imageDiv.addEventListener("click", () => {
            this.imageInput.click();
        });
    }

    addAttractionToList(event) {

        event.preventDefault(); //PARA DE RECARREGAR PAGINA QUANDO CLICA EM SUBMIT

        const attractionTitle = event.target["input-title-id"].value;
        const attractionDescription = event.target["input-description-id"].value;
        const attractionImage = event.target["image-id"].src;
        const testeImage = event.target["image-id"];

        if ((attractionTitle != "") && (attractionDescription != "") && (this.imageInput.value != "")){
            //CRIANDO OBJETO COM IMAGEM, TITULO E DESCRIÇÃO
            const attraction = {
                image: attractionImage,
                title: attractionTitle,
                description: attractionDescription,
            };

            this.list.push(attraction);
            this.renderListAttraction();
            this.clearInput();
        } else {
            alert("Preencha todos os campos");
        }
    }

    previewImage(event) {
        let file = event.target.files[0];
        let fileReader = new FileReader();

        fileReader.onloadend = () => {
            if (fileReader.onloadend != null) {
                this.classImage.setAttribute("src", fileReader.result);
                this.classImage.style.filter = "brightness(60%)";
                this.classImage.style.display = "block";
                this.placeholderImage.textContent = file.name;
                this.placeholderImage.style.color = "white";
            }
        };

        fileReader.readAsDataURL(file);
    }

    renderListAttraction() {
        let attractionsStructure = "";

        this.list.forEach(function (attraction) {
            attractionsStructure += `
            <li class="attraction">

                <img class="attraction-image" src=${attraction.image} />

                <div class="attraction-text">
                    <h3>${attraction.title}</h3>
                    <p>${attraction.description}</p>
                </div>

            </li>
            `;
        });

        this.attractions.innerHTML = attractionsStructure;
    }

    clearInput(event) {
        this.titleInput.value = "";
        this.descriptionInput.value = "";
        this.imageInput.value = "";
        this.classImage.setAttribute("src", "");
        this.classImage.style.display = "none";
        this.placeholderImage.style.display = "block";
        this.placeholderImage.style.color = "#858585";
        this.placeholderImage.textContent = "Imagem";
    }
}
